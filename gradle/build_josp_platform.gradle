/*******************************************************************************
 * The John Operating System Project is the collection of software and configurations
 * to generate IoT EcoSystem, like the John Operating System Platform one.
 * Copyright (C) 2021 Roberto Pompermaier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/

import com.robypomper.build.gradle.GradleAsyncTasksUtils
import com.robypomper.build.gradle.GradleAsyncTasksUtils.StartAsync
import com.robypomper.build.gradle.GradleAsyncTasksUtils.StopAsync


// -----------
// Async tasks
// -----------

// JCP APIs
String jcpAPIs = "jcpAPIs"
task jcpAPIs_StartAsync(type: StartAsync) {
    group 'JOSP runners jcp (soft)'

    command = "./gradlew jcpAPIs_Start"// --debug"
    //workingDir = project.file(workingDirBase + 'jcpAPIs_StartAsync')
    ready = 'Self connected'
    //suppressConsoleOutputs = true
    addLogFileStartTime = true
    logFile = file('envs/runnables/jcp/' + 'jcpAPIs_StartAsync/' + 'jcp.log')
    pidFile = file('envs/runnables/jcp/' + 'jcpAPIs_StartAsync/' + 'jcp.pid')
    throwOnError = false
}
jcpAPIs_StartAsync.dependsOn printGradleVersions
task jcpAPIs_StopAsync(type: StopAsync) {
    group 'JOSP runners jcp (soft)'

    pidFile = (jcpAPIs_StartAsync as StartAsync).pidFile
    throwOnError = false
}
task jcpAPIs_StatusAsync() {
    group 'JOSP runners jcp (soft)'

    doFirst {
        println 'jcpAPIs_Async task is ' + (jcpAPIs_StartAsync.isRunning() ? "running" : "NOT running")
    }
}
task jcpAPIs_CleanAsync(type: Delete) {
    group 'JOSP cleaners'
    delete "envs/runnables/jcp/jcpAPIs_StartAsync"

    //...delete and recreate jcp_api database in dbms
}
jcpAPIs_Clean.dependsOn jcpAPIs_CleanAsync

// JCP GWs
task jcpGWs_StartAsync(type: StartAsync) {
    group 'JOSP runners jcp (soft)'

    command = "./gradlew jcpGWs_Start"// --debug"
    //workingDir = project.file(workingDirBase + 'jcpGWs_StartAsync')
    ready = 'Self connected'
    //suppressConsoleOutputs = true
    addLogFileStartTime = true
    logFile = file('envs/runnables/jcp/' + 'jcpGWs_StartAsync/' + 'jcp.log')
    pidFile = file('envs/runnables/jcp/' + 'jcpGWs_StartAsync/' + 'jcp.pid')
    throwOnError = false
}
jcpGWs_StartAsync.dependsOn printGradleVersions
task jcpGWs_StopAsync(type: StopAsync) {
    group 'JOSP runners jcp (soft)'

    pidFile = (jcpGWs_StartAsync as StartAsync).pidFile
    throwOnError = false
}
task jcpGWs_StatusAsync() {
    group 'JOSP runners jcp (soft)'

    doFirst {
        println 'jcpGWs_Async task is ' + (jcpGWs_StartAsync.isRunning() ? "running" : "NOT running")
    }
}
task jcpGWs_CleanAsync(type: Delete) {
    group 'JOSP cleaners'
    delete "envs/runnables/jcp/jcpGWs_StartAsync"

    //...delete and recreate jcp_api database in dbms
}
jcpGWs_Clean.dependsOn jcpGWs_CleanAsync

// JSL Web Bridge
task jcpJSLWebBridge_StartAsync(type: StartAsync) {
    group 'JOSP runners jcp (soft)'

    command = "./gradlew jcpJSLWebBridge_Start"// --debug"
    //workingDir = project.file(workingDirBase + 'jcpJSLWebBridge_StartAsync')
    ready = 'Self connected'
    //suppressConsoleOutputs = true
    addLogFileStartTime = true
    logFile = file('envs/runnables/jcp/' + 'jcpJSLWebBridge_StartAsync/' + 'jcp.log')
    pidFile = file('envs/runnables/jcp/' + 'jcpJSLWebBridge_StartAsync/' + 'jcp.pid')
    throwOnError = false
}
jcpJSLWebBridge_StartAsync.dependsOn printGradleVersions
task jcpJSLWebBridge_StopAsync(type: StopAsync) {
    group 'JOSP runners jcp (soft)'

    pidFile = (jcpJSLWebBridge_StartAsync as StartAsync).pidFile
    throwOnError = false
}
task jcpJSLWebBridge_StatusAsync() {
    group 'JOSP runners jcp (soft)'

    doFirst {
        println 'jcpJSLWebBridge_Async task is ' + (jcpJSLWebBridge_StartAsync.isRunning() ? "running" : "NOT running")
    }
}
task jcpJSLWebBridge_CleanAsync(type: Delete) {
    group 'JOSP cleaners'
    delete "envs/runnables/jcp/jcpJSLWebBridge_StartAsync"

    //...delete and recreate jcp_api database in dbms
}
jcpJSLWebBridge_Clean.dependsOn jcpJSLWebBridge_CleanAsync

// JCP FE
task jcpFE_StartAsync(type: StartAsync) {
    group 'JOSP runners jcp (soft)'

    command = "./gradlew jcpFE_Start"// --debug"
    //workingDir = project.file(workingDirBase + 'jcpFE_StartAsync')
    ready = 'Self connected'
    //suppressConsoleOutputs = true
    addLogFileStartTime = true
    logFile = file('envs/runnables/jcp/' + 'jcpFE_StartAsync/' + 'jcp_fe.log')
    pidFile = file('envs/runnables/jcp/' + 'jcpFE_StartAsync/' + 'jcp_fe.pid')
    throwOnError = false
}
jcpFE_StartAsync.dependsOn printGradleVersions
task jcpFE_StopAsync(type: StopAsync) {
    group 'JOSP runners jcp (soft)'

    pidFile = (jcpFE_StartAsync as StartAsync).pidFile
    throwOnError = false
}
task jcpFE_StatusAsync() {
    group 'JOSP runners jcp (soft)'

    doFirst {
        println 'jcpFE_Async task is ' + (jcpFE_StartAsync.isRunning() ? "running" : "NOT running")
    }
}
task jcpFE_CleanAsync(type: Delete) {
    group 'JOSP cleaners'
    delete "envs/runnables/jcp/jcpFE_StartAsync"

    //...delete and recreate jcp_fe database in dbms
}
jcpFE_Clean.dependsOn jcpFE_CleanAsync

// JCP All
task jcpAll_StartAsync(type: StartAsync) {
    group 'JOSP runners jcp (soft)'

    command = "./gradlew jcpAll_Start"// --debug"
    //workingDir = project.file(workingDirBase + 'jcpAll_StartAsync')
    ready = 'Self connected'
    //suppressConsoleOutputs = true
    addLogFileStartTime = true
    logFile = file('envs/runnables/jcp/' + 'jcpAll_StartAsync/' + 'jcp_fe.log')
    pidFile = file('envs/runnables/jcp/' + 'jcpAll_StartAsync/' + 'jcp_fe.pid')
    throwOnError = false
}
jcpAll_StartAsync.dependsOn printGradleVersions
task jcpAll_StopAsync(type: StopAsync) {
    group 'JOSP runners jcp (soft)'

    pidFile = (jcpAll_StartAsync as StartAsync).pidFile
    throwOnError = false
}
task jcpAll_StatusAsync() {
    group 'JOSP runners jcp (soft)'

    doFirst {
        println 'jcpAll_Async task is ' + (jcpAll_StartAsync.isRunning() ? "running" : "NOT running")
    }
}
task jcpAll_CleanAsync(type: Delete) {
    group 'JOSP cleaners'
    delete "envs/runnables/jcp/jcpAll_StartAsync"

    //...delete and recreate jcp_fe database in dbms
}
jcpAll_Clean.dependsOn jcpAll_CleanAsync


// -------------------
// JOSP Platform tasks
// -------------------

int dbms_Up_WAIT_AFTER = 10
/**int jcpAPIs_StartAsync_WAIT_AFTER = 10*/
int jospCloudDockers_Start_WAIT_AFTER = 20

// Start cloud
task jospCloudDockers_Start {
    group 'JOSP runners Platform (dockers)'

    dependsOn dbms_Up, auth_Up
    GradleAsyncTasksUtils.waitAfter(dbms_Up as Task, dbms_Up_WAIT_AFTER, auth_Up as Task)
}
task jospCloudSoft_Start {
    group 'JOSP runners Platform (soft)'

    dependsOn jcpAPIs_StartAsync, jcpGWs_StartAsync, jcpJSLWebBridge_StartAsync, jcpFE_StartAsync
    GradleAsyncTasksUtils.waitAfter(jcpAPIs_StartAsync as Task, 0, jcpGWs_StartAsync as Task)
    GradleAsyncTasksUtils.waitAfter(jcpGWs_StartAsync as Task, 0, jcpJSLWebBridge_StartAsync as Task)
    GradleAsyncTasksUtils.waitAfter(jcpJSLWebBridge_StartAsync as Task, 0, jcpFE_StartAsync as Task)
}
task jospCloud_Start {
    group 'JOSP runners Platform'

    dependsOn jospCloudDockers_Start, jospCloudSoft_Start
    GradleAsyncTasksUtils.waitAfter(jospCloudDockers_Start as Task, jospCloudDockers_Start_WAIT_AFTER, jospCloudSoft_Start as Task)
}

// Stop cloud
task jospCloudDockers_Stop {
    group 'JOSP runners Platform (dockers)'

    dependsOn auth_Down, dbms_Down
    GradleAsyncTasksUtils.waitAfter(auth_Down as Task, 0, dbms_Down as Task)
}
task jospCloudSoft_Stop {
    group 'JOSP runners Platform (soft)'

    dependsOn jcpFE_StopAsync, jcpJSLWebBridge_StopAsync, jcpGWs_StopAsync, jcpAPIs_StopAsync
    GradleAsyncTasksUtils.waitAfter(jcpFE_StopAsync as Task, 0, jcpJSLWebBridge_StopAsync as Task)
    GradleAsyncTasksUtils.waitAfter(jcpJSLWebBridge_StopAsync as Task, 0, jcpGWs_StopAsync as Task)
    GradleAsyncTasksUtils.waitAfter(jcpGWs_StopAsync as Task, 0, jcpAPIs_StopAsync as Task)
}
task jospCloud_Stop {
    group 'JOSP runners Platform'

    dependsOn jospCloudSoft_Stop, jospCloudDockers_Stop
    GradleAsyncTasksUtils.waitAfter(jospCloudSoft_Stop as Task, 0, jospCloudDockers_Stop as Task)
}

// Clean cloud
task jospCloudDockers_Clean {
    group 'JOSP runners Platform (dockers)'

    dependsOn dbms_Clean, auth_Clean
}
task jospCloudSoft_Clean {
    group 'JOSP runners Platform (soft)'

    dependsOn jcpAPIs_CleanAsync, jcpJSLWebBridge_CleanAsync, jcpGWs_CleanAsync, jcpFE_CleanAsync
}
task jospCloud_Clean {
    group 'JOSP runners Platform'

    dependsOn jospCloudDockers_Clean, jospCloudSoft_Clean
}

// Restart cloud
task jospCloudDockers_Restart {
    group 'JOSP runners Platform (dockers)'

    dependsOn jospCloudDockers_Stop, jospCloudDockers_Start
    GradleAsyncTasksUtils.waitAfter(jospCloudDockers_Stop as Task, 0, jospCloudDockers_Start as Task)
}
task jospCloudSoft_Restart {
    group 'JOSP runners Platform (soft)'

    dependsOn jospCloudSoft_Stop, jospCloudSoft_Start
    GradleAsyncTasksUtils.waitAfter(jospCloudSoft_Stop as Task, 0, jospCloudSoft_Start as Task)
}
task jospCloud_Restart {
    group 'JOSP runners Platform'

    dependsOn jospCloudSoft_Stop, jospCloudDockers_Stop, jospCloudDockers_Start, jospCloudSoft_Start
    GradleAsyncTasksUtils.waitAfter(jospCloudSoft_Stop as Task, 0, jospCloudDockers_Stop as Task)
    GradleAsyncTasksUtils.waitAfter(jospCloudDockers_Stop as Task, 0, jospCloudDockers_Start as Task)
    GradleAsyncTasksUtils.waitAfter(jospCloudDockers_Start as Task, 0, jospCloudSoft_Start as Task)
}

// Restart and clean cloud
task jospCloudDockers_Refresh {
    group 'JOSP runners Platform (dockers)'

    dependsOn jospCloudDockers_Stop, jospCloudDockers_Clean, jospCloudDockers_Start
    GradleAsyncTasksUtils.waitAfter(jospCloudDockers_Stop as Task, 0, jospCloudDockers_Clean as Task)
    GradleAsyncTasksUtils.waitAfter(jospCloudDockers_Clean as Task, 0, jospCloudDockers_Start as Task)
}
task jospCloudSoft_Refresh {
    group 'JOSP runners Platform (soft)'

    dependsOn jospCloudSoft_Stop, jospCloudSoft_Clean, jospCloudSoft_Start
    GradleAsyncTasksUtils.waitAfter(jospCloudSoft_Stop as Task, 0, jospCloudSoft_Clean as Task)
    GradleAsyncTasksUtils.waitAfter(jospCloudSoft_Clean as Task, 0, jospCloudSoft_Start as Task)
}
task jospCloud_Refresh {
    group 'JOSP runners Platform'

    dependsOn jospCloudSoft_Stop, jospCloudDockers_Stop, jospCloudDockers_Clean, jospCloudSoft_Clean, jospCloudDockers_Start, jospCloudSoft_Start
    GradleAsyncTasksUtils.waitAfter(jospCloudSoft_Stop as Task, 0, jospCloudDockers_Stop as Task)
    GradleAsyncTasksUtils.waitAfter(jospCloudDockers_Stop as Task, 0, jospCloudDockers_Clean as Task)
    GradleAsyncTasksUtils.waitAfter(jospCloudDockers_Clean as Task, 0, jospCloudSoft_Clean as Task)
    GradleAsyncTasksUtils.waitAfter(jospCloudSoft_Clean as Task, 0, jospCloudDockers_Start as Task)
    GradleAsyncTasksUtils.waitAfter(jospCloudDockers_Start as Task, 0, jospCloudSoft_Start as Task)
}


// ------------------
// Eco-Systems tasks
// ------------------

// Avvia Local Eco-System + JOD + JSL
// JOD (only local) + JSL (only local)

// Avvia Cloud Eco-System + JOD + JSL
// JCP All + JOD (only cloud) + JSL (only cloud)

// Avvia Full Eco-System + JOD + JSL
// JCP All + JOD + JSL

// Clean
//???
